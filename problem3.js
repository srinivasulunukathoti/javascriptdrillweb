// Assume that each salary amount is a factor of 10000 and correct it but add it as a new key (corrected_salary or something)

function eachSalaryAmountFactor(data) {
    let result = data.map(developer => {
        if (developer.salary !==null) {
            let salaries = Number(developer.salary.replace("$",""));
            let correctedSalry = salaries*10000;
             developer.corrected_Salary = correctedSalry;
        }
        return developer;
    });
    return result;
};
module.exports = {eachSalaryAmountFactor};
