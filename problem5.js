// Find the sum of all salaries based on country. ( Group it based on country and then find the sum ).

function findSumOfAllSalariesBasedOnCountry(data) {
    let countrySum = data.reduce((acc, developer) => {
        // Check if the location property exists and is not null
        if (developer.location !== null && developer.salary !== null) {
            // Remove "$" and convert salary to a number
            let salary = Number(developer.salary.replace("$","").replace(",",""));
            
            // Check if the country already exists in the accumulator object
            if (acc[developer.location] !== undefined) {
                // If it exists, add the salary to the existing sum
                acc[developer.location] += salary;
            } else {
                // If it doesn't exist, initialize the sum for that country
                acc[developer.location] = salary;
            }
        }
        return acc;
    }, {});

    return countrySum;       
};
module.exports = {findSumOfAllSalariesBasedOnCountry};