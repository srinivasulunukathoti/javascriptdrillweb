const findSumOfAllSalariesBasedOnCountry = require('../problem5');
const data = require('../data');

try {
    const result = findSumOfAllSalariesBasedOnCountry.findSumOfAllSalariesBasedOnCountry(data.data);
    console.log(result);
} catch (error) {
    console.log("data not found");
}