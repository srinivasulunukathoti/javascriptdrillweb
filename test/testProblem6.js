const averageSalaryBasedOnCountry = require('../problem6');
const data = require('../data');

try {
    const result = averageSalaryBasedOnCountry.averageSalaryBasedOnCountry(data.data);
    console.log(result);
} catch (error) {
    console.log("data not found");
}