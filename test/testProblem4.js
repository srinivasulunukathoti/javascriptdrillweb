const sumAllSalaries = require('../problem4');
const data = require('../data');

try {
    const salaries = sumAllSalaries.sumAllSalaries(data.data);
    console.log(salaries);
} catch (error) {
    console.log("data not found");
}