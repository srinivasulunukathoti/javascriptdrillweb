const findAllWebDevelopers = require('../problem1');
const data = require('../data');

try {
    const webDeveloper = findAllWebDevelopers.findAllWebDevelopers(data.data);
    console.log(webDeveloper);
} catch (error) {
    console.log("Insufficiant data");
}