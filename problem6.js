// Find the average salary of based on country. ( Groupd it based on country and then find the average ).

 function averageSalaryBasedOnCountry(data) {
    let countryStats = data.reduce((acc, developer) => {
        if (developer.location !== null && developer.salary !== null) {
            let salary = Number(developer.salary.replace("$","").replace(",",""));
            
            if (acc[developer.location] !== undefined) {
                acc[developer.location].sum += salary;
                acc[developer.location].count++;
            } else {
                acc[developer.location] = { sum: salary, count: 1 };
            }
        }
        return acc;
    }, {});

    let averageSalaryByCountry = {};
    for (let country in countryStats) {
        averageSalaryByCountry[country] = countryStats[country].sum / countryStats[country].count;
    }

    return averageSalaryByCountry;
 }
 module.exports = {averageSalaryBasedOnCountry};