// Find the sum of all salaries.

function sumAllSalaries(data) {
    let result = data.reduce((sumOfSalaries, developer) => {
        if (developer.salary !== null) {
            let salary = Number(developer.salary.replace("$","").replace(",",""));
            sumOfSalaries += salary;
        }
        return sumOfSalaries;
    }, 0); // Initialize sumOfSalaries as 0
    return result;
}

module.exports = { sumAllSalaries };