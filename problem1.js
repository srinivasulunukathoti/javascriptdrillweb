// Find all Web Developers. ( It could be Web Developer III or Web Developer II or anything else )

function findAllWebDevelopers(data) {
    let webDevelopers = data.filter(job =>{
        if (job.job === "Web Developer II" ||job.job === "Web Developer III" ||job.job === "Web Developer") {
            return job;
        }
    });
    return webDevelopers || null;
}
module.exports = {findAllWebDevelopers};