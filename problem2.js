// Convert all the salary values into proper numbers instead of strings.

function convertAllSalariesProperNumber(data) {
    let result = data.map(job =>
        {
            if (job.salary !==null) {
                return Number(job.salary.replace("$",""));
            }
        });
        return result;
}
module.exports = {convertAllSalariesProperNumber};